%include "lib.inc"
section .text
global find_word

find_word:
 
    push r12
    push r13
    
    ; Инициализация локальных переменных (в регистрах)
    mov r12, rdi  ; r12 для искомого слова
    mov r13, rsi  ; r13 для начала списка
    
    ; Цикл поиска
    .start_loop:
        ; Проверка на конец списка
        test r13, r13
        je .failure_exit
        add r13, 8   ; Смещаемся к строке в узле
        
        ; Вызываем string_equals
        mov rdi, r12
        mov rsi, r13
        call string_equals
        test rax, rax
        jnz .success_exit

        ; Переходим к следующему узлу
        sub r13, 8
        mov r13, [r13]
        jmp .start_loop
    
    .failure_exit:
        xor rax, rax
        pop r13
        pop r12
        ret
    
    .success_exit:
        mov rax, r13
        pop r13
        pop r12
        ret
        
        
