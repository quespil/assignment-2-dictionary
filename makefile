ASM=nasm
ASFLAGS=-f elf64
LD=ld
SRC=main.asm lib.asm dict.asm
OBJ=$(SRC:.asm=.o)
TARGET=program
PYTHON=python3
SCRIPT=test.py

.PHONY: clean all

all: $(TARGET)

$(TARGET): $(OBJ)
	$(LD) -o $@ $^

# Общее правило для компиляции .asm файла
%.o: %.asm
	$(ASM) $(ASFLAGS) $< -o $@

# Явные зависимости
main.o: main.asm dict.inc lib.inc words.inc 
dict.o: dict.asm lib.inc
lib.o: lib.asm

clean:
	rm -f $(OBJ) $(TARGET)

test: $(TARGET)
	$(PYTHON) $(SCRIPT)

rebuild: clean all

