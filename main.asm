%include "dict.inc"
%include "lib.inc"
%include "words.inc"
%define BUFFER_SIZE 256


section .bss
buffer resb BUFFER_SIZE  ; Буфер для ввода строки

section .rodata
    read_err_msg db "[ERROR]: error when reading a word!", 0
    not_found_msg db "[ERROR]: can not found this word in dict", 0

section .text
    global _start
    
_start:
    mov rdi, buffer  ; Указатель на буфер
    mov rsi, BUFFER_SIZE     ; Размер буфера
    call read_word
    test rax, rax
    jz .read_error
    
    
    
    mov rdi, buffer
    mov rsi, last_pointer
    call find_word
    test rax, rax
    jz .not_found ; go to print error if not found

    mov rdi, rax
    add rdi, 8 ; add size_of_cell
    push rdi
    call string_length
    pop rdi
    add rdi, rax ; add to go to the value pointer
    inc rdi ; skip null-terminator
    call print_string
    call print_newline
    
    xor rdi, rdi
    ret

    
    
    .read_error:
        mov rdi, read_err_msg
        jmp .print_error_msg

   .not_found:
        mov rdi, not_found_msg

   .print_error_msg:
        call print_error
        jmp exit

