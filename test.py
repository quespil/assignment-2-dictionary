import subprocess

data = ["first_word", "second_word", "third_word", "", "jhqwqqwfqwfqwffasdghjqghhwrqjhgqwhjqwghjqgqwipjerghqwrasdffasdfhdsfasdfjsdfkjsdfjl;jf;sdfjkfiauioguioweriquioqugqweghuiopqewhgqwehgqwehghqweiguhqweiughqweuighqweipguhqweuipgwqegqwgqwggqwegAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAbebebe"]
stdout_answer = ["ia v djakuzi 1", "shlapi dapi 2", "this is full kaif 3", "", ""]

stderr_answer = ["", "", "", "[ERROR]: can not found this word in dict", "[ERROR]: error when reading a word!"]

path = "./program"
count = 0
for key, value in enumerate(data):
    process = subprocess.run(path, input=value, text=True, capture_output=True)

    stdout = process.stdout.strip()
    stderr = process.stderr.strip()
    if stdout != stdout_answer[key]:
        print(f"test{key+1} failed: stdout: {stdout}\n but expected {stdout_answer[key]}")
    if stderr != stderr_answer[key]:
        print(f"test{key+1} failed: stdout {stderr}\n but expected {stderr_answer[key]}")
    else:
        print(f"test{key+1} passed!")
        count += 1


    print("<<<<<<<<<<<<<<>>>>>>>>>>>>>>")
print(f'tests passed: {len(data)}/{count}')

