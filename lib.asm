global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.string_loop:
    cmp byte[rax+rdi], 0
    je .end_of_string
    inc rax
    jmp .string_loop
.end_of_string:
	ret


print_string:
    xor rax, rax
    push rdi
    call string_length
    mov rdx, rax        
    pop rsi
    mov rax, 1           
    mov rdi, 1           
    syscall
    ret

print_error:
    xor rax, rax
    push rdi
    call string_length
    mov rdx, rax
    pop rsi
    mov rax, 1
    mov rdi, 2
    syscall
    jmp print_newline
    

; Принимает код символа и выводит его в stdout

print_char:
    xor rax, rax
    sub rsp, 1
    mov byte [rsp], dil
    mov rsi, rsp    
    mov rax, 1      
    mov rdi, 1   
    mov rdx, 1      
    syscall
    add rsp, 1
    ret


; Переводит строку (выводит символ с кодом 0xA)   
print_newline:
    mov rdi, 0xA
    jmp print_char


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rbx
    push rsi
    xor rax,rax
    sub rsp, 20

    lea rsi, [rsp + 19]
    mov rax, rdi 
    xor rcx, rcx
.loop:
    ; Делим число на 10
    xor rdx, rdx 
    mov rbx, 10 
    div rbx 
    add dl, '0' 
    dec rsi 
    mov [rsi], dl
    inc rcx
    test rax, rax
    je .end_loop

    jmp .loop

.end_loop:

    mov rax, 1      
    mov rdi, 1     
    mov rdx, rcx      
    syscall

    add rsp, 20
    pop rsi
    pop rbx
    ret






; Выводит знаковое 8-байтовое число в десятичном формате 
; Функция выводит знаковое 8-байтовое число в десятичном формате
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi    
    jns .is_positive      

    push rdi
    mov rax, 1        
    mov rdi, 1            
    mov rdx, 1           
    sub rsp, 1            
    mov byte [rsp], '-'   
    mov rsi, rsp          

    syscall               

    add rsp, 1           
    pop rdi
    neg rdi              
.is_positive:
    call print_uint     
    ret    


  





; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе

string_equals:
    xor rax, rax
.loop:
    mov al, byte [rdi]
    cmp al, byte [rsi]
    jne .not_equals
    
    test al, al
    je .equals   
    inc rdi
    inc rsi
    jmp .loop
    
  
.not_equals:
    xor rax, rax
    ret

.equals:
    mov rax, 1
    ret
 





; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax         
    xor rdi, rdi          
    dec rsp   
    mov rax, 0     
    mov rdi, 0       
    mov rsi, rsp   
    mov rdx, 1         
    syscall            
    test rax, rax
    jz .eof
    mov al, byte[rsp]
    inc rsp     
    ret
.eof:
    xor rax, rax    
    add rsp, 1     
    ret
	

	
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rbx
    xor rbx, rbx
    push r12
    push r13
    push r14
    
    mov r12, rbx
    mov r13, rdi
    mov r14, rsi

.loope:
    cmp r12, r14
    jae .error 
    call read_char
    test rax, rax
    je .success
    
    cmp rax, 0x20
    je .tabs
    cmp rax, 0x9
    je .tabs
    cmp rax, 0xA
    je .tabs
    
    mov [r13 + r12], rax
    inc r12
    
    jmp .loope
    
    
.success:
    mov byte [r13 + r12], 0
    mov rax, r13
    mov rdx, r12
    pop r14
    pop r13
    pop r12
    pop rbx
    ret
       

.error:
    xor rax, rax
    pop r14
    pop r13
    pop r12
    pop rbx 
    ret
    
.tabs:
    test r12, r12
    jne .success
    jmp .loope




 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push rbx
    xor rax, rax
    xor rbx, rbx
    xor rdx, rdx
    test rdi, rdi
    jz .done

.loop:

    movzx rbx, byte [rdi + rdx] 
    test rbx, rbx           
    jz .done
    sub rbx, '0'
    cmp rbx, 9
    ja .done
    imul rax, rax, 10
    add rax, rbx  
    inc rdx  
    jmp .loop

.done:
    test rdx, rdx
    jz .clear_rdx
    pop rbx
    ret
.clear_rdx:
    xor rax, rax
    xor rdx, rdx
    pop rbx
    ret   


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    push rdi  
    mov al, byte [rdi]  
    cmp al, '-'  
    jne .positive  
    inc rdi  
    call parse_uint  
    test rdx, rdx
    jz .clear_rdx
    neg rax  
    inc rdx  
    pop rdi  
    ret

.positive:
    call parse_uint  
    test rdx, rdx  
    jnz .done  

.clear_rdx:
    xor rax, rax
    xor rdx, rdx

.done:
    pop rdi 
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx 
.loop:
    mov al, byte [rdi + rcx]
    test al, al
    jz .done
    cmp rcx, rdx
    jae .buffer_too_small
    mov byte [rsi + rcx], al
    inc rcx
    jmp .loop

.buffer_too_small:
    xor rax, rax
    xor rcx, rcx
    jmp .restore_and_return

.done:
    mov byte [rsi + rcx], 0
    mov rax, rcx

.restore_and_return:
    ret
